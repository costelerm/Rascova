import React, { useState } from 'react'
import './App.css'
import 'tailwindcss/dist/tailwind.min.css'
import { TabelCopii } from './components/tabel-copii/TabelCopii'

function App() {
  const [count, setCount] = useState(0)

  return (
    <div>
         <TabelCopii />
    </div>
  )
}

export default App
