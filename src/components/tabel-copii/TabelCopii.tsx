import React from "react";
import { TabelCopiiData } from "./TabelCopiiData";
import "tailwindcss/dist/tailwind.min.css";

export function TabelCopii() {
  // return <div>{TabelCopiiData.map((x) => (<div><div className='bg-red-500 text-white text-center'>{x.nume + ' ' + x.prenume}</div>
  // <div className='bg-gray-500'>---</div></div>))}</div>

  function compareName(a: any, b: any) {
    // converting to uppercase to have case-insensitive comparison
    const name1 = a.nume.toUpperCase();
    const name2 = b.nume.toUpperCase();

    let comparison = 0;

    if (name1 > name2) {
      comparison = 1;
    } else if (name1 < name2) {
      comparison = -1;
    }
    return comparison;
  }

  return (
    <tbody className="bg-white divide-y divide-gray-200">
      {TabelCopiiData.sort(compareName).map((x) => (
        <tr>
          <td className="px-6 py-4 w-72">
            <div className="flex items-center">
              <div className="mr-4">{x.id}</div>
              <div className="flex-shrink-0 h-16 w-16">
                <img
                  className="h-16 w-16 rounded-full hover:"
                  src={x.foto}
                  alt={x.nume}
                />
              </div>
              <div className="ml-4">
                <div className="text-sm font-medium text-gray-900 whitespace-nowrap">
                  {x.nume + " " + x.prenume}
                </div>
                <div className="text-sm text-gray-500">
                  {x.ziua + " " + x.luna + " " + x.anul}
                </div>
                <div className="text-sm text-gray-500">{x.adresa}</div>
              </div>
            </div>
          </td>
          <td className="px-2 py-4 w-48 items-start">
            <div className="text-sm text-gray-900">{x.scoala}</div>
            <div className="text-sm text-gray-500">Clasa a {x.clasa}-a</div>
          </td>
          <td className="px-2 py-4 w-48 items-start">
            <div className="text-sm text-gray-500 whitespace-nowrap">
              {"mama: " + x.mama}
            </div>
            <div className="text-sm text-gray-500">{"tata: " + x.tata}</div>
            <div className="text-sm text-gray-500">
              {x.frati && "frati: " + x.frati}
            </div>
          </td>

          <td className="px-6 py-4 w-24">
            <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
              {x.telefon}
            </span>
          </td>
          <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
            {x.altele}
          </td>
        </tr>
      ))}
    </tbody>
  );
}
